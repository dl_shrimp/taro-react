import { Component } from "react";
import { View, Text, Image } from "@tarojs/components";
import { AtButton } from "taro-ui";
import "./index.scss";

interface IImage {
  id: number;
  url: string;
}
interface IState {
  dataList: IImage[];
}

export default class Index extends Component<any, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      dataList: []
    };
  }
  componentWillMount() {}

  componentDidMount() {
    let imgList: IImage[] = [];
    for (let i = 1; i < 15; i++) {
      imgList.push({
        id: i,
        url:
          "https://tse1-mm.cn.bing.net/th/id/R-C.4d9cd2e53dddfc238a06e750b73cd023?rik=MsMCKPGumufOyQ&riu=http%3a%2f%2fwww.desktx.com%2fd%2ffile%2fwallpaper%2fscenery%2f20170209%2fc2accfe637f86fb6f11949cb8651a09b.jpg&ehk=ia2TVXcow6ygWUVZ1yod5xH4aGd8565SYn6CRpxkNoo%3d&risl=&pid=ImgRaw&r=0"
      });
    }
    console.log(imgList);

    this.setState({ dataList: imgList });
  }

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  render() {
    const { dataList } = this.state;
    const imgUrl =
      "https://tse1-mm.cn.bing.net/th/id/R-C.4d9cd2e53dddfc238a06e750b73cd023?rik=MsMCKPGumufOyQ&riu=http%3a%2f%2fwww.desktx.com%2fd%2ffile%2fwallpaper%2fscenery%2f20170209%2fc2accfe637f86fb6f11949cb8651a09b.jpg&ehk=ia2TVXcow6ygWUVZ1yod5xH4aGd8565SYn6CRpxkNoo%3d&risl=&pid=ImgRaw&r=0";

    return (
      <View className="index">
        {dataList.map((item, index) => {
          return (
            <View key={item.id}>
              <Image mode="aspectFit" src={item.url}></Image>
            </View>
          );
        })}
      </View>
    );
  }
}
