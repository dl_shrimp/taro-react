export default {
  pages: ["pages/index/index", "pages/users/index", "pages/detail/index"],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black"
  },
  tabBar: {
    color: "#8a8a8a",
    selectedColor: "#FF0000",
    backgroundColor: "#fafafa",
    borderStyle: "black",
    list: [
      {
        pagePath: "pages/index/index",
        iconPath: "./static/tabBar/首页.png",
        selectedIconPath: "./static/tabBar/首页 (1).png",
        text: "首页"
      },
      {
        pagePath: "pages/users/index",
        iconPath: "./static/tabBar/分类.png",
        selectedIconPath: "./static/tabBar/分类 (1).png",
        text: "分类"
      },
      {
        pagePath: "pages/detail/index",
        iconPath: "./static/tabBar/人事管理.png",
        selectedIconPath: "./static/tabBar/人事管理 (1).png",
        text: "我的"
      }
    ]
  }
};
